Microsoft Windows [Version 10.0.14393]
(c) 2016 Microsoft Corporation. All rights reserved.

C:\Users\chris> mysql -h localhost -u root -p
Enter password:
Welcome to the MariaDB monitor.  Commands end with ; or \g.
Your MariaDB connection id is 222
Server version: 10.4.17-MariaDB mariadb.org binary distribution

Copyright (c) 2000, 2018, Oracle, MariaDB Corporation Ab and others.

Type 'help;' or '\h' for help. Type '\c' to clear the current input statement.

MariaDB [(none)]> SHOW DATABASES;
+--------------------+
| Database           |
+--------------------+
| blog_db            |
| information_schema |
| music_db           |
| mysql              |
| performance_schema |
| phpmyadmin         |
| test               |
+--------------------+
7 rows in set (0.001 sec)

MariaDB [(none)]> USE music_db;
Database changed
MariaDB [music_db]> SHOW TABLES;
+--------------------+
| Tables_in_music_db |
+--------------------+
| albums             |
| artists            |
| playlists          |
| playlists_songs    |
| songs              |
| users              |
+--------------------+
6 rows in set (0.001 sec)

MariaDB [music_db]> DESCRIBE albums;
+-----------+-------------+------+-----+---------+----------------+
| Field     | Type        | Null | Key | Default | Extra          |
+-----------+-------------+------+-----+---------+----------------+
| id        | int(11)     | NO   | PRI | NULL    | auto_increment |
| name      | varchar(50) | NO   |     | NULL    |                |
| year      | date        | NO   |     | NULL    |                |
| artist_id | int(11)     | NO   | MUL | NULL    |                |
+-----------+-------------+------+-----+---------+----------------+
4 rows in set (0.167 sec)

MariaDB [music_db]> SELECT * FROM albums;
+----+-----------------+------------+-----------+
| id | name            | year       | artist_id |
+----+-----------------+------------+-----------+
|  1 | Psy 6           | 2012-01-01 |         2 |
|  2 | Trip            | 1996-01-01 |         1 |
| 13 | Fearless        | 2008-01-01 |         3 |
| 14 | Red             | 2012-02-02 |         3 |
| 15 | A Star Is Born  | 2018-03-03 |         4 |
| 16 | Born This Way   | 2011-04-04 |         4 |
| 17 | Purpose         | 2015-05-05 |         5 |
| 18 | Believe         | 2012-06-06 |         5 |
| 19 | Dangerous Woman | 2016-07-07 |         6 |
| 20 | Thank U, Next   | 2019-08-08 |         6 |
| 21 | 24K Magic       | 2016-09-09 |         7 |
| 22 | Earth to Mars   | 2011-10-10 |         7 |
+----+-----------------+------------+-----------+
12 rows in set (0.001 sec)

MariaDB [music_db]> DESCRIBE artists;
+-------+-------------+------+-----+---------+----------------+
| Field | Type        | Null | Key | Default | Extra          |
+-------+-------------+------+-----+---------+----------------+
| id    | int(11)     | NO   | PRI | NULL    | auto_increment |
| name  | varchar(50) | NO   |     | NULL    |                |
+-------+-------------+------+-----+---------+----------------+
2 rows in set (0.186 sec)

MariaDB [music_db]> SELECT * FROM artists;
+----+---------------+
| id | name          |
+----+---------------+
|  1 | Rivermaya     |
|  2 | Psy           |
|  3 | Taylor Swift  |
|  4 | Lady  Gaga    |
|  5 | Justin Bieber |
|  6 | Ariana Grande |
|  7 | Bruno Mars    |
+----+---------------+
7 rows in set (0.001 sec)

MariaDB [music_db]> DESCRIBE playlists;
+--------------+----------+------+-----+---------+----------------+
| Field        | Type     | Null | Key | Default | Extra          |
+--------------+----------+------+-----+---------+----------------+
| id           | int(11)  | NO   | PRI | NULL    | auto_increment |
| date_created | datetime | NO   |     | NULL    |                |
| user_id      | int(11)  | NO   | MUL | NULL    |                |
+--------------+----------+------+-----+---------+----------------+
3 rows in set (0.058 sec)

MariaDB [music_db]> SELECT * FROM playlists;
Empty set (0.000 sec)

MariaDB [music_db]> DESCRIBE playlists_songs;
+-------------+---------+------+-----+---------+----------------+
| Field       | Type    | Null | Key | Default | Extra          |
+-------------+---------+------+-----+---------+----------------+
| id          | int(11) | NO   | PRI | NULL    | auto_increment |
| playlist_id | int(11) | NO   | MUL | NULL    |                |
| song_id     | int(11) | NO   |     | NULL    |                |
+-------------+---------+------+-----+---------+----------------+
3 rows in set (0.194 sec)

MariaDB [music_db]> SELECT * FROM playlists_songs;
Empty set (0.001 sec)

MariaDB [music_db]> DESCRIBE songs;
+----------+-------------+------+-----+---------+----------------+
| Field    | Type        | Null | Key | Default | Extra          |
+----------+-------------+------+-----+---------+----------------+
| id       | int(11)     | NO   | PRI | NULL    | auto_increment |
| title    | varchar(50) | NO   |     | NULL    |                |
| length   | time        | NO   |     | NULL    |                |
| genre    | varchar(50) | NO   |     | NULL    |                |
| album_id | int(11)     | NO   | MUL | NULL    |                |
+----------+-------------+------+-----+---------+----------------+
5 rows in set (0.160 sec)

MariaDB [music_db]> SELECT * FROM songs;
+----+----------------+----------+---------------------------------------+----------+
| id | title          | length   | genre                                 | album_id |
+----+----------------+----------+---------------------------------------+----------+
|  1 | Gangnam Style  | 00:02:53 | K-Pop                                 |        1 |
|  2 | Kundiman       | 00:02:40 | OPM                                   |        2 |
|  3 | Kisapmata      | 00:03:19 | OPM                                   |        2 |
|  4 | Fearless       | 00:02:46 | Pop rock                              |       13 |
|  5 | Love Story     | 00:02:13 | Country pop                           |       13 |
|  6 | State of Grace | 00:02:13 | Alternative Rock                      |       14 |
|  7 | Red            | 00:02:04 | Country                               |       14 |
|  8 | Black Eyes     | 00:02:21 | Rock and roll                         |       15 |
|  9 | Shallow        | 00:02:01 | Country, rock, folk rock              |       15 |
| 10 | Born This Way  | 00:02:52 | Electropop                            |       16 |
| 11 | Sorry          | 00:02:32 | Dancehall-poptropical housemoombahton |       17 |
| 12 | Boyfriend      | 00:02:51 | Pop                                   |       18 |
| 13 | Into You       | 00:02:42 | EDM house                             |       19 |
| 14 | Thank U Next   | 00:02:36 | Pop, R&B                              |       20 |
| 15 | 24K Magic      | 00:02:07 | Funk, disco, R&B                      |       21 |
| 16 | Lost           | 00:02:32 | Pop                                   |       22 |
+----+----------------+----------+---------------------------------------+----------+
16 rows in set (0.001 sec)

MariaDB [music_db]> DESCRIBE users;
+----------+-------------+------+-----+---------+----------------+
| Field    | Type        | Null | Key | Default | Extra          |
+----------+-------------+------+-----+---------+----------------+
| id       | int(11)     | NO   | PRI | NULL    | auto_increment |
| username | varchar(50) | NO   |     | NULL    |                |
| password | varchar(50) | NO   |     | NULL    |                |
+----------+-------------+------+-----+---------+----------------+
3 rows in set (0.166 sec)

MariaDB [music_db]> SELECT * FROM users;
Empty set (0.001 sec)

Microsoft Windows [Version 10.0.14393]
(c) 2016 Microsoft Corporation. All rights reserved.

C:\Users\chris> mysql -h localhost -u root -p
Enter password:
Welcome to the MariaDB monitor.  Commands end with ; or \g.
Your MariaDB connection id is 8
Server version: 10.4.17-MariaDB mariadb.org binary distribution

Copyright (c) 2000, 2018, Oracle, MariaDB Corporation Ab and others.

Type 'help;' or '\h' for help. Type '\c' to clear the current input statement.

MariaDB [(none)]> SHOW DATABASES;
+--------------------+
| Database           |
+--------------------+
| blog_db            |
| information_schema |
| music_db           |
| mysql              |
| performance_schema |
| phpmyadmin         |
| test               |
+--------------------+
7 rows in set (0.254 sec)

MariaDB [(none)]> USE music_db;
Database changed
MariaDB [music_db]> SHOW TABLES;
+--------------------+
| Tables_in_music_db |
+--------------------+
| albums             |
| artists            |
| playlists          |
| playlists_songs    |
| songs              |
| users              |
+--------------------+
6 rows in set (0.054 sec)

MariaDB [music_db]> SELECT * FROM artists;
+----+---------------+
| id | name          |
+----+---------------+
|  1 | Rivermaya     |
|  2 | Psy           |
|  3 | Taylor Swift  |
|  4 | Lady  Gaga    |
|  5 | Justin Bieber |
|  6 | Ariana Grande |
|  7 | Bruno Mars    |
+----+---------------+
7 rows in set (0.373 sec)

MariaDB [music_db]> SELECT * FROM artists WHERE id = 4;
+----+------------+
| id | name       |
+----+------------+
|  4 | Lady  Gaga |
+----+------------+
1 row in set (0.363 sec)

MariaDB [music_db]> SELECT * FROM artists WHERE id = 4;
+----+------------+
| id | name       |
+----+------------+
|  4 | Lady  Gaga |
+----+------------+
1 row in set (0.001 sec)

MariaDB [music_db]> SELECT * FROM artists WHERE id = 6;
+----+---------------+
| id | name          |
+----+---------------+
|  6 | Ariana Grande |
+----+---------------+
1 row in set (0.001 sec)

MariaDB [music_db]> SELECT * FROM songs;
+----+----------------+----------+---------------------------------------+----------+
| id | title          | length   | genre                                 | album_id |
+----+----------------+----------+---------------------------------------+----------+
|  1 | Gangnam Style  | 00:02:53 | K-Pop                                 |        1 |
|  2 | Kundiman       | 00:02:40 | OPM                                   |        2 |
|  3 | Kisapmata      | 00:03:19 | OPM                                   |        2 |
|  4 | Fearless       | 00:02:46 | Pop rock                              |       13 |
|  5 | Love Story     | 00:02:13 | Country pop                           |       13 |
|  6 | State of Grace | 00:02:13 | Alternative Rock                      |       14 |
|  7 | Red            | 00:02:04 | Country                               |       14 |
|  8 | Black Eyes     | 00:02:21 | Rock and roll                         |       15 |
|  9 | Shallow        | 00:02:01 | Country, rock, folk rock              |       15 |
| 10 | Born This Way  | 00:02:52 | Electropop                            |       16 |
| 11 | Sorry          | 00:02:32 | Dancehall-poptropical housemoombahton |       17 |
| 12 | Boyfriend      | 00:02:51 | Pop                                   |       18 |
| 13 | Into You       | 00:02:42 | EDM house                             |       19 |
| 14 | Thank U Next   | 00:02:36 | Pop, R&B                              |       20 |
| 15 | 24K Magic      | 00:02:07 | Funk, disco, R&B                      |       21 |
| 16 | Lost           | 00:02:32 | Pop                                   |       22 |
+----+----------------+----------+---------------------------------------+----------+
16 rows in set (0.176 sec)

MariaDB [music_db]> SELECT * FROM artists WHERE id = 4 OR id = 6;
+----+---------------+
| id | name          |
+----+---------------+
|  4 | Lady  Gaga    |
|  6 | Ariana Grande |
+----+---------------+
2 rows in set (0.069 sec)

MariaDB [music_db]> SELECT * FROM songs;
+----+----------------+----------+---------------------------------------+----------+
| id | title          | length   | genre                                 | album_id |
+----+----------------+----------+---------------------------------------+----------+
|  1 | Gangnam Style  | 00:02:53 | K-Pop                                 |        1 |
|  2 | Kundiman       | 00:02:40 | OPM                                   |        2 |
|  3 | Kisapmata      | 00:03:19 | OPM                                   |        2 |
|  4 | Fearless       | 00:02:46 | Pop rock                              |       13 |
|  5 | Love Story     | 00:02:13 | Country pop                           |       13 |
|  6 | State of Grace | 00:02:13 | Alternative Rock                      |       14 |
|  7 | Red            | 00:02:04 | Country                               |       14 |
|  8 | Black Eyes     | 00:02:21 | Rock and roll                         |       15 |
|  9 | Shallow        | 00:02:01 | Country, rock, folk rock              |       15 |
| 10 | Born This Way  | 00:02:52 | Electropop                            |       16 |
| 11 | Sorry          | 00:02:32 | Dancehall-poptropical housemoombahton |       17 |
| 12 | Boyfriend      | 00:02:51 | Pop                                   |       18 |
| 13 | Into You       | 00:02:42 | EDM house                             |       19 |
| 14 | Thank U Next   | 00:02:36 | Pop, R&B                              |       20 |
| 15 | 24K Magic      | 00:02:07 | Funk, disco, R&B                      |       21 |
| 16 | Lost           | 00:02:32 | Pop                                   |       22 |
+----+----------------+----------+---------------------------------------+----------+
16 rows in set (0.001 sec)

MariaDB [music_db]> SELECT * FROM songs WHERE id = 5 OR id = 6 OR id = 7 OR id = 8 OR id = 9 OR id = 15;
+----+----------------+----------+--------------------------+----------+
| id | title          | length   | genre                    | album_id |
+----+----------------+----------+--------------------------+----------+
|  5 | Love Story     | 00:02:13 | Country pop              |       13 |
|  6 | State of Grace | 00:02:13 | Alternative Rock         |       14 |
|  7 | Red            | 00:02:04 | Country                  |       14 |
|  8 | Black Eyes     | 00:02:21 | Rock and roll            |       15 |
|  9 | Shallow        | 00:02:01 | Country, rock, folk rock |       15 |
| 15 | 24K Magic      | 00:02:07 | Funk, disco, R&B         |       21 |
+----+----------------+----------+--------------------------+----------+
6 rows in set (0.001 sec)

MariaDB [music_db]> SELECT * FROM albums;
+----+-----------------+------------+-----------+
| id | name            | year       | artist_id |
+----+-----------------+------------+-----------+
|  1 | Psy 6           | 2012-01-01 |         2 |
|  2 | Trip            | 1996-01-01 |         1 |
| 13 | Fearless        | 2008-01-01 |         3 |
| 14 | Red             | 2012-02-02 |         3 |
| 15 | A Star Is Born  | 2018-03-03 |         4 |
| 16 | Born This Way   | 2011-04-04 |         4 |
| 17 | Purpose         | 2015-05-05 |         5 |
| 18 | Believe         | 2012-06-06 |         5 |
| 19 | Dangerous Woman | 2016-07-07 |         6 |
| 20 | Thank U, Next   | 2019-08-08 |         6 |
| 21 | 24K Magic       | 2016-09-09 |         7 |
| 22 | Earth to Mars   | 2011-10-10 |         7 |
+----+-----------------+------------+-----------+
12 rows in set (0.143 sec)

Microsoft Windows [Version 10.0.14393]
(c) 2016 Microsoft Corporation. All rights reserved.

C:\Users\chris> mysql -h localhost -u root -p
Enter password:
Welcome to the MariaDB monitor.  Commands end with ; or \g.
Your MariaDB connection id is 8
Server version: 10.4.17-MariaDB mariadb.org binary distribution

Copyright (c) 2000, 2018, Oracle, MariaDB Corporation Ab and others.

Type 'help;' or '\h' for help. Type '\c' to clear the current input statement.

MariaDB [(none)]> SHOW DATABASES;
+--------------------+
| Database           |
+--------------------+
| blog_db            |
| information_schema |
| music_db           |
| mysql              |
| performance_schema |
| phpmyadmin         |
| test               |
+--------------------+
7 rows in set (0.254 sec)

MariaDB [(none)]> USE music_db;
Database changed
MariaDB [music_db]> SHOW TABLES;
+--------------------+
| Tables_in_music_db |
+--------------------+
| albums             |
| artists            |
| playlists          |
| playlists_songs    |
| songs              |
| users              |
+--------------------+
6 rows in set (0.054 sec)

MariaDB [music_db]> SELECT * FROM artists;
+----+---------------+
| id | name          |
+----+---------------+
|  1 | Rivermaya     |
|  2 | Psy           |
|  3 | Taylor Swift  |
|  4 | Lady  Gaga    |
|  5 | Justin Bieber |
|  6 | Ariana Grande |
|  7 | Bruno Mars    |
+----+---------------+
7 rows in set (0.373 sec)

MariaDB [music_db]> SELECT * FROM artists WHERE id = 4 & 6;
+----+------------+
| id | name       |
+----+------------+
|  4 | Lady  Gaga |
+----+------------+
1 row in set (0.363 sec)

MariaDB [music_db]> SELECT * FROM artists WHERE id = 4;
+----+------------+
| id | name       |
+----+------------+
|  4 | Lady  Gaga |
+----+------------+
1 row in set (0.001 sec)

MariaDB [music_db]> SELECT * FROM artists WHERE id = 6;
+----+---------------+
| id | name          |
+----+---------------+
|  6 | Ariana Grande |
+----+---------------+
1 row in set (0.001 sec)

MariaDB [music_db]> SELECT * FROM songs;
+----+----------------+----------+---------------------------------------+----------+
| id | title          | length   | genre                                 | album_id |
+----+----------------+----------+---------------------------------------+----------+
|  1 | Gangnam Style  | 00:02:53 | K-Pop                                 |        1 |
|  2 | Kundiman       | 00:02:40 | OPM                                   |        2 |
|  3 | Kisapmata      | 00:03:19 | OPM                                   |        2 |
|  4 | Fearless       | 00:02:46 | Pop rock                              |       13 |
|  5 | Love Story     | 00:02:13 | Country pop                           |       13 |
|  6 | State of Grace | 00:02:13 | Alternative Rock                      |       14 |
|  7 | Red            | 00:02:04 | Country                               |       14 |
|  8 | Black Eyes     | 00:02:21 | Rock and roll                         |       15 |
|  9 | Shallow        | 00:02:01 | Country, rock, folk rock              |       15 |
| 10 | Born This Way  | 00:02:52 | Electropop                            |       16 |
| 11 | Sorry          | 00:02:32 | Dancehall-poptropical housemoombahton |       17 |
| 12 | Boyfriend      | 00:02:51 | Pop                                   |       18 |
| 13 | Into You       | 00:02:42 | EDM house                             |       19 |
| 14 | Thank U Next   | 00:02:36 | Pop, R&B                              |       20 |
| 15 | 24K Magic      | 00:02:07 | Funk, disco, R&B                      |       21 |
| 16 | Lost           | 00:02:32 | Pop                                   |       22 |
+----+----------------+----------+---------------------------------------+----------+
16 rows in set (0.176 sec)

MariaDB [music_db]> SELECT * FROM artists WHERE id = 4 OR id = 6;
+----+---------------+
| id | name          |
+----+---------------+
|  4 | Lady  Gaga    |
|  6 | Ariana Grande |
+----+---------------+
2 rows in set (0.069 sec)

MariaDB [music_db]> SELECT * FROM songs;
+----+----------------+----------+---------------------------------------+----------+
| id | title          | length   | genre                                 | album_id |
+----+----------------+----------+---------------------------------------+----------+
|  1 | Gangnam Style  | 00:02:53 | K-Pop                                 |        1 |
|  2 | Kundiman       | 00:02:40 | OPM                                   |        2 |
|  3 | Kisapmata      | 00:03:19 | OPM                                   |        2 |
|  4 | Fearless       | 00:02:46 | Pop rock                              |       13 |
|  5 | Love Story     | 00:02:13 | Country pop                           |       13 |
|  6 | State of Grace | 00:02:13 | Alternative Rock                      |       14 |
|  7 | Red            | 00:02:04 | Country                               |       14 |
|  8 | Black Eyes     | 00:02:21 | Rock and roll                         |       15 |
|  9 | Shallow        | 00:02:01 | Country, rock, folk rock              |       15 |
| 10 | Born This Way  | 00:02:52 | Electropop                            |       16 |
| 11 | Sorry          | 00:02:32 | Dancehall-poptropical housemoombahton |       17 |
| 12 | Boyfriend      | 00:02:51 | Pop                                   |       18 |
| 13 | Into You       | 00:02:42 | EDM house                             |       19 |
| 14 | Thank U Next   | 00:02:36 | Pop, R&B                              |       20 |
| 15 | 24K Magic      | 00:02:07 | Funk, disco, R&B                      |       21 |
| 16 | Lost           | 00:02:32 | Pop                                   |       22 |
+----+----------------+----------+---------------------------------------+----------+
16 rows in set (0.001 sec)

MariaDB [music_db]> SELECT * FROM songs WHERE id = 5 OR id = 6 OR id = 7 OR id = 8 OR id = 9 OR id = 15;
+----+----------------+----------+--------------------------+----------+
| id | title          | length   | genre                    | album_id |
+----+----------------+----------+--------------------------+----------+
|  5 | Love Story     | 00:02:13 | Country pop              |       13 |
|  6 | State of Grace | 00:02:13 | Alternative Rock         |       14 |
|  7 | Red            | 00:02:04 | Country                  |       14 |
|  8 | Black Eyes     | 00:02:21 | Rock and roll            |       15 |
|  9 | Shallow        | 00:02:01 | Country, rock, folk rock |       15 |
| 15 | 24K Magic      | 00:02:07 | Funk, disco, R&B         |       21 |
+----+----------------+----------+--------------------------+----------+
6 rows in set (0.001 sec)

MariaDB [music_db]> SELECT * FROM albums;
+----+-----------------+------------+-----------+
| id | name            | year       | artist_id |
+----+-----------------+------------+-----------+
|  1 | Psy 6           | 2012-01-01 |         2 |
|  2 | Trip            | 1996-01-01 |         1 |
| 13 | Fearless        | 2008-01-01 |         3 |
| 14 | Red             | 2012-02-02 |         3 |
| 15 | A Star Is Born  | 2018-03-03 |         4 |
| 16 | Born This Way   | 2011-04-04 |         4 |
| 17 | Purpose         | 2015-05-05 |         5 |
| 18 | Believe         | 2012-06-06 |         5 |
| 19 | Dangerous Woman | 2016-07-07 |         6 |
| 20 | Thank U, Next   | 2019-08-08 |         6 |
| 21 | 24K Magic       | 2016-09-09 |         7 |
| 22 | Earth to Mars   | 2011-10-10 |         7 |
+----+-----------------+------------+-----------+
12 rows in set (0.143 sec)

MariaDB [music_db]> SELECT * FROM songs;
+----+----------------+----------+---------------------------------------+----------+
| id | title          | length   | genre                                 | album_id |
+----+----------------+----------+---------------------------------------+----------+
|  1 | Gangnam Style  | 00:02:53 | K-Pop                                 |        1 |
|  2 | Kundiman       | 00:02:40 | OPM                                   |        2 |
|  3 | Kisapmata      | 00:03:19 | OPM                                   |        2 |
|  4 | Fearless       | 00:02:46 | Pop rock                              |       13 |
|  5 | Love Story     | 00:02:13 | Country pop                           |       13 |
|  6 | State of Grace | 00:02:13 | Alternative Rock                      |       14 |
|  7 | Red            | 00:02:04 | Country                               |       14 |
|  8 | Black Eyes     | 00:02:21 | Rock and roll                         |       15 |
|  9 | Shallow        | 00:02:01 | Country, rock, folk rock              |       15 |
| 10 | Born This Way  | 00:02:52 | Electropop                            |       16 |
| 11 | Sorry          | 00:02:32 | Dancehall-poptropical housemoombahton |       17 |
| 12 | Boyfriend      | 00:02:51 | Pop                                   |       18 |
| 13 | Into You       | 00:02:42 | EDM house                             |       19 |
| 14 | Thank U Next   | 00:02:36 | Pop, R&B                              |       20 |
| 15 | 24K Magic      | 00:02:07 | Funk, disco, R&B                      |       21 |
| 16 | Lost           | 00:02:32 | Pop                                   |       22 |
+----+----------------+----------+---------------------------------------+----------+
16 rows in set (0.001 sec)

MariaDB [music_db]> SELECT artists.name, albums.name FROM artists JOIN albums ON artists.id = albums.artist_id;
+---------------+-----------------+
| name          | name            |
+---------------+-----------------+
| Rivermaya     | Trip            |
| Psy           | Psy 6           |
| Taylor Swift  | Fearless        |
| Taylor Swift  | Red             |
| Lady  Gaga    | A Star Is Born  |
| Lady  Gaga    | Born This Way   |
| Justin Bieber | Purpose         |
| Justin Bieber | Believe         |
| Ariana Grande | Dangerous Woman |
| Ariana Grande | Thank U, Next   |
| Bruno Mars    | 24K Magic       |
| Bruno Mars    | Earth to Mars   |
+---------------+-----------------+
12 rows in set (0.053 sec)

MariaDB [music_db]> SHOW DATABASES;
+--------------------+
| Database           |
+--------------------+
| blog_db            |
| classic_models     |
| information_schema |
| music_db           |
| mysql              |
| performance_schema |
| phpmyadmin         |
| test               |
+--------------------+
8 rows in set (0.001 sec)

MariaDB [music_db]> SHOW TABLES;
+--------------------+
| Tables_in_music_db |
+--------------------+
| albums             |
| artists            |
| playlists          |
| playlists_songs    |
| songs              |
| users              |
+--------------------+
6 rows in set (0.001 sec)

MariaDB [music_db]> SELECT * FROM artists WHERE name LIKE "%d%";
+----+---------------+
| id | name          |
+----+---------------+
|  4 | Lady  Gaga    |
|  6 | Ariana Grande |
+----+---------------+
2 rows in set (0.001 sec)

MariaDB [music_db]> SELECT * FROM songs WHERE length < 230;
+----+----------------+----------+--------------------------+----------+
| id | title          | length   | genre                    | album_id |
+----+----------------+----------+--------------------------+----------+
|  5 | Love Story     | 00:02:13 | Country pop              |       13 |
|  6 | State of Grace | 00:02:13 | Alternative Rock         |       14 |
|  7 | Red            | 00:02:04 | Country                  |       14 |
|  8 | Black Eyes     | 00:02:21 | Rock and roll            |       15 |
|  9 | Shallow        | 00:02:01 | Country, rock, folk rock |       15 |
| 15 | 24K Magic      | 00:02:07 | Funk, disco, R&B         |       21 |
+----+----------------+----------+--------------------------+----------+
6 rows in set (0.124 sec)

MariaDB [music_db]> SELECT * FROM albums;
+----+-----------------+------------+-----------+
| id | name            | year       | artist_id |
+----+-----------------+------------+-----------+
|  1 | Psy 6           | 2012-01-01 |         2 |
|  2 | Trip            | 1996-01-01 |         1 |
| 13 | Fearless        | 2008-01-01 |         3 |
| 14 | Red             | 2012-02-02 |         3 |
| 15 | A Star Is Born  | 2018-03-03 |         4 |
| 16 | Born This Way   | 2011-04-04 |         4 |
| 17 | Purpose         | 2015-05-05 |         5 |
| 18 | Believe         | 2012-06-06 |         5 |
| 19 | Dangerous Woman | 2016-07-07 |         6 |
| 20 | Thank U, Next   | 2019-08-08 |         6 |
| 21 | 24K Magic       | 2016-09-09 |         7 |
| 22 | Earth to Mars   | 2011-10-10 |         7 |
+----+-----------------+------------+-----------+
12 rows in set (0.001 sec)

MariaDB [music_db]> SELECT * FROM songs;
+----+----------------+----------+---------------------------------------+----------+
| id | title          | length   | genre                                 | album_id |
+----+----------------+----------+---------------------------------------+----------+
|  1 | Gangnam Style  | 00:02:53 | K-Pop                                 |        1 |
|  2 | Kundiman       | 00:02:40 | OPM                                   |        2 |
|  3 | Kisapmata      | 00:03:19 | OPM                                   |        2 |
|  4 | Fearless       | 00:02:46 | Pop rock                              |       13 |
|  5 | Love Story     | 00:02:13 | Country pop                           |       13 |
|  6 | State of Grace | 00:02:13 | Alternative Rock                      |       14 |
|  7 | Red            | 00:02:04 | Country                               |       14 |
|  8 | Black Eyes     | 00:02:21 | Rock and roll                         |       15 |
|  9 | Shallow        | 00:02:01 | Country, rock, folk rock              |       15 |
| 10 | Born This Way  | 00:02:52 | Electropop                            |       16 |
| 11 | Sorry          | 00:02:32 | Dancehall-poptropical housemoombahton |       17 |
| 12 | Boyfriend      | 00:02:51 | Pop                                   |       18 |
| 13 | Into You       | 00:02:42 | EDM house                             |       19 |
| 14 | Thank U Next   | 00:02:36 | Pop, R&B                              |       20 |
| 15 | 24K Magic      | 00:02:07 | Funk, disco, R&B                      |       21 |
| 16 | Lost           | 00:02:32 | Pop                                   |       22 |
+----+----------------+----------+---------------------------------------+----------+
16 rows in set (0.001 sec)

MariaDB [music_db]> SELECT albums.name, songs.title, songs.length FROM albums JOIN songs ON albums.id = songs.album_id;
+-----------------+----------------+----------+
| name            | title          | length   |
+-----------------+----------------+----------+
| Psy 6           | Gangnam Style  | 00:02:53 |
| Trip            | Kundiman       | 00:02:40 |
| Trip            | Kisapmata      | 00:03:19 |
| Fearless        | Fearless       | 00:02:46 |
| Fearless        | Love Story     | 00:02:13 |
| Red             | State of Grace | 00:02:13 |
| Red             | Red            | 00:02:04 |
| A Star Is Born  | Black Eyes     | 00:02:21 |
| A Star Is Born  | Shallow        | 00:02:01 |
| Born This Way   | Born This Way  | 00:02:52 |
| Purpose         | Sorry          | 00:02:32 |
| Believe         | Boyfriend      | 00:02:51 |
| Dangerous Woman | Into You       | 00:02:42 |
| Thank U, Next   | Thank U Next   | 00:02:36 |
| 24K Magic       | 24K Magic      | 00:02:07 |
| Earth to Mars   | Lost           | 00:02:32 |
+-----------------+----------------+----------+
16 rows in set (0.001 sec)

MariaDB [music_db]> SELECT * FROM artists JOIN albums ON artists.id = albums.artist_id WHERE albums.name LIKE "%a%";
+----+---------------+----+-----------------+------------+-----------+
| id | name          | id | name            | year       | artist_id |
+----+---------------+----+-----------------+------------+-----------+
|  3 | Taylor Swift  | 13 | Fearless        | 2008-01-01 |         3 |
|  4 | Lady  Gaga    | 15 | A Star Is Born  | 2018-03-03 |         4 |
|  4 | Lady  Gaga    | 16 | Born This Way   | 2011-04-04 |         4 |
|  6 | Ariana Grande | 19 | Dangerous Woman | 2016-07-07 |         6 |
|  6 | Ariana Grande | 20 | Thank U, Next   | 2019-08-08 |         6 |
|  7 | Bruno Mars    | 21 | 24K Magic       | 2016-09-09 |         7 |
|  7 | Bruno Mars    | 22 | Earth to Mars   | 2011-10-10 |         7 |
+----+---------------+----+-----------------+------------+-----------+
7 rows in set (0.001 sec)

MariaDB [music_db]> SELECT * FROM albums ORDER BY name DESC LIMIT 4;
+----+---------------+------------+-----------+
| id | name          | year       | artist_id |
+----+---------------+------------+-----------+
|  2 | Trip          | 1996-01-01 |         1 |
| 20 | Thank U, Next | 2019-08-08 |         6 |
| 14 | Red           | 2012-02-02 |         3 |
| 17 | Purpose       | 2015-05-05 |         5 |
+----+---------------+------------+-----------+
4 rows in set (0.001 sec)

MariaDB [music_db]> SELECT * FROM albums JOIN songs ON albums.id = songs.album_id ORDER by albums.name DESC, songs.title ASC;
+----+-----------------+------------+-----------+----+----------------+----------+---------------------------------------+----------+
| id | name            | year       | artist_id | id | title          | length   | genre                                 | album_id |
+----+-----------------+------------+-----------+----+----------------+----------+---------------------------------------+----------+
|  2 | Trip            | 1996-01-01 |         1 |  3 | Kisapmata      | 00:03:19 | OPM                                   |        2 |
|  2 | Trip            | 1996-01-01 |         1 |  2 | Kundiman       | 00:02:40 | OPM                                   |        2 |
| 20 | Thank U, Next   | 2019-08-08 |         6 | 14 | Thank U Next   | 00:02:36 | Pop, R&B                              |       20 |
| 14 | Red             | 2012-02-02 |         3 |  7 | Red            | 00:02:04 | Country                               |       14 |
| 14 | Red             | 2012-02-02 |         3 |  6 | State of Grace | 00:02:13 | Alternative Rock                      |       14 |
| 17 | Purpose         | 2015-05-05 |         5 | 11 | Sorry          | 00:02:32 | Dancehall-poptropical housemoombahton |       17 |
|  1 | Psy 6           | 2012-01-01 |         2 |  1 | Gangnam Style  | 00:02:53 | K-Pop                                 |        1 |
| 13 | Fearless        | 2008-01-01 |         3 |  4 | Fearless       | 00:02:46 | Pop rock                              |       13 |
| 13 | Fearless        | 2008-01-01 |         3 |  5 | Love Story     | 00:02:13 | Country pop                           |       13 |
| 22 | Earth to Mars   | 2011-10-10 |         7 | 16 | Lost           | 00:02:32 | Pop                                   |       22 |
| 19 | Dangerous Woman | 2016-07-07 |         6 | 13 | Into You       | 00:02:42 | EDM house                             |       19 |
| 16 | Born This Way   | 2011-04-04 |         4 | 10 | Born This Way  | 00:02:52 | Electropop                            |       16 |
| 18 | Believe         | 2012-06-06 |         5 | 12 | Boyfriend      | 00:02:51 | Pop                                   |       18 |
| 15 | A Star Is Born  | 2018-03-03 |         4 |  8 | Black Eyes     | 00:02:21 | Rock and roll                         |       15 |
| 15 | A Star Is Born  | 2018-03-03 |         4 |  9 | Shallow        | 00:02:01 | Country, rock, folk rock              |       15 |
| 21 | 24K Magic       | 2016-09-09 |         7 | 15 | 24K Magic      | 00:02:07 | Funk, disco, R&B                      |       21 |
+----+-----------------+------------+-----------+----+----------------+----------+---------------------------------------+----------+
16 rows in set (0.002 sec)

MariaDB [music_db]> SHOW DATABASES;
+--------------------+
| Database           |
+--------------------+
| blog_db            |
| classic_models     |
| information_schema |
| music_db           |
| mysql              |
| performance_schema |
| phpmyadmin         |
| test               |
+--------------------+
8 rows in set (0.003 sec)

MariaDB [music_db]> SHOW TABLES;
+--------------------+
| Tables_in_music_db |
+--------------------+
| albums             |
| artists            |
| playlists          |
| playlists_songs    |
| songs              |
| users              |
+--------------------+
6 rows in set (0.001 sec)

MariaDB [music_db]>
